/*
DECLARE ARROW FUNCTIONS HERE
*/
//sortable fix that keeps columns from collapsing
const fixHelper = (e, ui) => {
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
};

//changes number to new rowIndex
const rowChange = () => {
    $('tr', '#imgData').each((index, element) => {
        $(element).children('td').eq(3).html('<p id="imgNumber">' + parseInt(index + 1) + '</p>')
    });
};

const dragenter = (e) => {
    e.stopPropagation();
    e.preventDefault();
};


const dragover = (e) => {
    e.stopPropagation();
    e.preventDefault();
};

const droppics = (e) => {
    e.stopPropagation();
    e.preventDefault();
    let dt = e.dataTransfer;
    let files = dt.files;
    handleFiles(files);
};

const handleFiles = (files) => {
    for (let i = 0; i < files.length; i++) {
        let file = files[i];
        let imageType = /^image\//;

        //test if image, then test if zip, if neither do nothing
        if (imageType.test(file.type)) {
            addImage(file);
        } else if (file.name.indexOf(".zip") !== -1) {
            loadZip(file);
        } else if (file.name.indexOf(".csv") !== -1) {
            alert('It looks like you\'re trying to upload a CSV file.  ' +
                'Recent updates to the photolog tool have put the csv file into the ZIP during download.  ' +
                'If you have an old save file you want to load, please put the CSV file into the zip and then drop ' +
                'the zip file into the tool.')
        }
    }
};

//Process data
const loadData = (file, zip) => {
    const reader = new FileReader();
    const data = file.asText();
    const blob = new Blob([data]);
    reader.onload = (e) => {
        let csv = e.target.result;
        let csvData = $.csv.toArrays(csv);
        //Project infos
        $('#inputClient').val(csvData[0][1]);
        $('#inputSiteName').val(csvData[1][1]);
        $('#inputProjectNumber').val(csvData[2][1]);
        $('#inputSiteLocation').val(csvData[3][1]);
        //Skip 4 - that's the headers
        //Picture infos
        for (let i = 5; i < csvData.length; i++) {
            //insert new row and add picture
            addPicRow();
            let imgData = document.getElementById('imgData');
            let row = imgData.rows[imgData.rows.length - 1];
            //add data stored in CSV
            let imgName = document.createElement("p");
            imgName.setAttribute("class", "hidden");
            imgName.setAttribute("id", "imgName");
            let imgFileName = csvData[i][0];
            imgName.innerHTML = imgFileName;
            $(row).find('#imgDiv').append(imgName);
            // Add image
            if (zip.file(imgFileName)) {
                imgProcess(new Blob([zip.file(imgFileName).asArrayBuffer()]), row);
            } else {
                console.warn('The zip does not contain', imgFileName)
            }
            $(row).find('#imgRatio').text(csvData[i][1]);
            $(row).find('#imgAngle').text(csvData[i][2]);
            $(row).find('#imgNumber').text(csvData[i][3]);
            $(row).find('#imgDate').val(csvData[i][4]);
            $(row).find('#imgDir').val(csvData[i][5]);
            $(row).find('#imgComment').text(csvData[i][6]);
        }
    };
    reader.onerror = (evt) => {
        if (evt.target.error.name === "NotReadableError") {
            alert("Can't read file!");
        }
    };
    reader.readAsText(blob);
};

//Open and Process zip file putting images where they go
function loadZip(file) {
    const reader = new FileReader();
    reader.onload =(() => {
        return function (e) {
            const zip = new JSZip(e.target.result);
            $.each(zip.files, function (index, file) {
                const file_ext = file.name.split('.').pop();
                if (file_ext === 'csv') {
                    // Load CSV data first so that the picture rows exist for images to go into
                    loadData(file, zip);
                }
            });
        };
    })(file);
    reader.readAsArrayBuffer(file);
}

const addPicRow = () => {
    const imgData = document.getElementById('imgData');
    const row = imgData.insertRow();
    const imgPre = row.insertCell(0);
    const imgRatio = row.insertCell(1);
    const imgAngle = row.insertCell(2);
    const imgNumber = row.insertCell(3);
    const imgDate = row.insertCell(4);
    const imgDir = row.insertCell(5);
    const imgComm = row.insertCell(6);
    const imgDel = row.insertCell(7);

    //add a div inside the img cell to specify max height and width of previews
    const imgDiv = document.createElement("div");
    imgDiv.setAttribute("class", "imgDiv");
    imgDiv.setAttribute("id", "imgDiv");
    imgPre.appendChild(imgDiv);

    //image rotation
    const imgRot = document.createElement("img");
    imgRot.setAttribute("class", "imgRot");
    imgRot.setAttribute("id", "imgRot");
    imgRot.setAttribute("src", "https://www.rwalker.info/assets/rotate_right-128.png");
    imgDiv.appendChild(imgRot);

    //store ratio in this element
    imgRatio.setAttribute('class', 'hidden');
    imgRatio.innerHTML = "<p id='imgRatio' class ='hidden'></p>";
    //store image angle in this element
    imgAngle.setAttribute('class', 'hidden');
    imgAngle.innerHTML = "<p id='imgAngle' class ='hidden'>0</p>";
    //Assign default image number
    imgNumber.innerHTML = "<p id='imgNumber'>" + row.rowIndex + "</p>";
    //image date
    imgDate.innerHTML = "<input id='imgDate' type='date'> </input>";
    //Direction select drop-down
    imgDir.innerHTML = "<select id='imgDir'>" +
        "<option value='N'>N</option>" +
        "<option value='NE'>NE</option>" +
        "<option value='E'>E</option>" +
        "<option value='SE'>SE</option>" +
        "<option value='S'>S</option>" +
        "<option value='SW'>SW</option>" +
        "<option value='W'>W</option>" +
        "<option value='NW'>NW</option>" +
        "<option value='N/A'>N/A</option>" +
        "</select>";

    imgComm.innerHTML = "<textarea id='imgComment' class='comment' rows='1' cols='30' ></textarea>";
    //delete button
    imgDel.innerHTML = "<img id='delete' src='https://www.rwalker.info/assets/delete.png' width='50px' height='50px'/>";
};

const addImage = (file) => {
    addPicRow();
    //create table to show users photo information and order
    const imgData = document.getElementById('imgData');
    const row = imgData.rows[imgData.rows.length - 1];

    //Gets image from file- appends new row in table

    //image name - used for saves
    const imgName = document.createElement("p");
    $(imgName).addClass("hidden");
    $(imgName).attr("id", "imgName");
    $(imgName).text(file.name);
    $(row).find('#imgDiv').append(imgName);

    //Assign default image number
    $(row).find('#imgNumber').text(row.rowIndex);
    //Convert Date Modified of File to MM/DD/YYYY format
    //TODO - allow customizable date formats? low priority
    const imgDateTime = moment(file.lastModifiedDate).format("YYYY-MM-DD");
    $(row).find('#imgDate').val(imgDateTime);
    imgProcess(file, row);
};

const imgProcess = (imgBlob, row) => {
    //adds file source to table as object - can call this later when making the log
    const reader = new FileReader();

    reader.onload = ((aImg) => {
        return function (e) {
            const blob = new Blob([e.target.result]);
            window.URL = window.URL || window.webkitURL;
            const blobURL = window.URL.createObjectURL(blob);

            const image = new Image();

            image.onload = () => {
                aImg.prepend(resizeMe(image));
                aImg.children('canvas').css(
                    {
                        'max-height': '100%',
                        'max-width': '100%',
                        'position': 'absolute',
                        'margin': 'auto',
                        'top': 0,
                        'left': 0,
                        'right': 0,
                        'bottom': 0
                    });
            };
            image.src = blobURL;
        }
    })($(row).find('#imgDiv'));
    reader.readAsArrayBuffer(imgBlob);
};

const resizeMe = (img) => {
    const canvas = document.createElement('canvas');

    let width = img.width;
    let height = img.height;
    let max_width = 1080;
    let max_height = 1080;
    // calculate the width and height, constraining the proportions
    if (width > height) {
        if (width > max_width) {
            //height *= max_width / width;
            height = Math.round(height * (max_width / width));
            width = max_width;
        }
    } else {
        if (height > max_height) {
            //width *= max_height / height;
            width = Math.round(width * (max_height / height));
            height = max_height;
        }
    }

    // resize the canvas and draw the image data into it
    canvas.width = width;
    canvas.height = height;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, width, height);
    return canvas;
};

const rotateMe = (img, angle) => {

    angle = angle || 90;
    const rc = clonePhoto(img);

    const width = img.width;
    const height = img.height;
    rc.width = height;
    rc.height = width;

    const ctx = rc.getContext("2d");
    ctx.translate(height / 2, height / 2);
    ctx.rotate(angle * Math.PI / 180);
    ctx.drawImage(img, -height / 2, -height / 2);

    return rc
};

//Clones photograph from table to use in log out put
const clonePhoto = (photo) => {
    //create a new canvas
    const newCanvas = document.createElement('canvas');
    const context = newCanvas.getContext('2d');

    //set dimensions
    newCanvas.width = photo.width;
    newCanvas.height = photo.height;

    //apply the old canvas to the new one
    context.drawImage(photo, 0, 0);

    //return the new canvas
    return newCanvas;
};

//Creates Log using Standard Geosyntec Template
const createLog = () => {

    // Word Document
    const loadFile = (url, callback) => {
        JSZipUtils.getBinaryContent(url, callback);
    };

    let docdata = {};
    docdata.photos = [];
    let picpage = {};
    //loop over data and add to modal
    $('tr', '#imgData').each((index, element) => {
        let $this = $(element);
        const rowNum = index + 1;
        let cP;
        if (rowNum % 2 !== 0) {
            cP = clonePhoto($this.find('canvas')[0]);
            picpage.photo = $this.find('#imgNumber').text();
            picpage.date = moment($this.find('#imgDate').val()).format("M/D/YYYY");
            picpage.direction = $this.find('#imgDir').val();
            picpage.comments = $this.find('#imgComment').val();
            picpage.image = cP.toDataURL();
        }
        // for every second photo do this....
        else {
            cP = clonePhoto($this.find('canvas')[0]);
            picpage.photo2 = $this.find('#imgNumber').text();
            picpage.date2 = moment($this.find('#imgDate').val()).format("M/D/YYYY");
            picpage.direction2 = $this.find('#imgDir').val();
            picpage.comments2 = $this.find('#imgComment').val();
            picpage.image2 = cP.toDataURL();
        }
        if (rowNum % 2 === 0 || rowNum === $('tr', '#imgData').length) {
            docdata.photos.push(picpage);
            picpage = {}; // clear object to prevent duplicates with odd number of pictures
        }
    });

    docdata.client = $('#inputClient').val();
    docdata.sitename = $('#inputSiteName').val();
    docdata.sitelocation = $('#inputSiteLocation').val();
    docdata.projectnumber = $('#inputProjectNumber').val();

    // Helper functions for image processing
    const base64DataURLToArrayBuffer = (dataURL) => {
        const stringBase64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
        let binaryString;
        if (typeof window !== "undefined") {
            binaryString = window.atob(stringBase64);
        }
        else {
            binaryString = new Buffer(stringBase64, "base64").toString("binary");
        }
        const len = binaryString.length;
        const bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
            bytes[i] = binaryString.charCodeAt(i);
        }
        return bytes.buffer;
    };

    const getPngDimensions = (base64) => {
        const header = base64.slice(0, 50);
        const uint8 = Uint8Array.from(atob(header), c => c.charCodeAt(0));
        const dataView = new DataView(uint8.buffer, 0, 28);

        return {
            width: dataView.getInt32(16),
            height: dataView.getInt32(20)
        }
    };

    let opts = {};
    opts.centered = true;
    opts.getImage = function (image) {
        image = base64DataURLToArrayBuffer(image);
        return image;
    };
    opts.getSize = function (img, tagValue) {
        let dimensions = getPngDimensions(tagValue.split(',')[1]);
        const pratio = dimensions.width / dimensions.height;
        if (pratio > 1) {
            if (dimensions.width > 350) {
                dimensions.width = 350;
                dimensions.height = 350 / pratio;
            } else {
                dimensions.height = 280;
                dimensions.width = 280 * pratio;
            }
        } else {
            dimensions.height = 280;
            dimensions.width = 280 * pratio;
        }
        return [dimensions.width, dimensions.height];
    };
    const imageModule = new ImageModule(opts);
    loadFile("https://www.rwalker.info/assets/Photolog Template.docx", (error, content) => {
        if (error) {
            throw error
        }
        const zip = new JSZip(content);
        const doc = new Docxtemplater().attachModule(imageModule).loadZip(zip);

        doc.setData(docdata);

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            const e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties
            };
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        const out = doc.getZip().generate({
            type: "blob",
            mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        });
        //Output the document using Data-URI
        saveAs(out, "PhotoLog.docx")
    })
};

const exportTableToCSV = ($table, filename) => {
    const $rows = $table.find('tr:has(td),tr:has(th)'),
        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        tmpColDelim = String.fromCharCode(11), // vertical tab character
        tmpRowDelim = String.fromCharCode(0), // null character

        // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"';

    //Get client, Site Name, etc.
    let csv = '"Client",' + $("#inputClient").val() + "\n" +
        '"Site Name",' + $("#inputSiteName").val() + "\n" +
        '"Project Number",' + $("#inputProjectNumber").val() + "\n" +
        '"Site Location",' + $("#inputSiteLocation").val() + "\n";

    //start new zip file
    const zip = new JSZip();

    // Grab text from table into CSV formatted string
    csv = csv + '"' + $rows.map((i, row) => {
        const $row = $(row), $cols = $row.find('td,th');

        return $cols.map((j, col) => {
            //Skip delete row
            if (j === 7) {
                return;
            }
            const $col = $(col);
            let text;
            //If first row, get headers
            if (i === 0) {
                text = $col.text();
            } else { //get data
                switch (j) {
                    //images
                    case 0:
                        text = $col.find('p')[0].innerHTML; //img name saved to CSV
                        //Img file saved to zip
                        let imgData = $col.find('canvas')[0];
                        const img = imgData.toDataURL();
                        const idx = img.indexOf(",");
                        imgData = img.substring(idx + 1, img.length);
                        zip.file(text, imgData, {base64: true});
                        break;
                    //image ratio
                    case 1:
                        text = $col.text();
                        break;
                    //image angle
                    case 2:
                        text = $col.text();
                        break;
                    //Image Number
                    case 3:
                        text = $col.text();
                        break;
                    //Image Date
                    case 4:
                        text = $col[0].firstChild.value;
                        break;
                    //Image Direction
                    case 5:
                        text = $col[0].firstChild.value;
                        break;
                    //Comments
                    case 6:
                        text = $col[0].firstChild.value;
                        break;
                }
            }
            return text.replace(/"/g, '""'); // escape double quotes

        }).get().join(tmpColDelim);

    }).get().join(tmpRowDelim)
        .split(tmpRowDelim).join(rowDelim)
        .split(tmpColDelim).join(colDelim) + '"';

    // Add CSV to zip
    zip.file(filename, csv);
    //Generate Zip file of Images
    const imgzip = zip.generate({type: "blob"});
    saveAs(imgzip, filename + ".zip");
};

$(document).ready(function () {

    /*
    EVENTS TO TRIGGER FUNCTIONS
     */

    //Prevent user from accidently opening image instead of dropping into table
    window.addEventListener("dragover", function (e) {
        e = e || event;
        e.preventDefault();
    }, false);
    window.addEventListener("drop", function (e) {
        e = e || event;
        e.preventDefault();
    }, false);

    //check that browser supports HTML5 - if not warn and exit
    if (window.FileReader) {
        // FileReader are supported.
    } else {
        alert('FileReader are not supported in this browser.');
    }

    //Event listeners for dropping pics
    let dropbox = document.getElementById("dropbox");
    dropbox.addEventListener("dragenter", dragenter, false);
    dropbox.addEventListener("dragover", dragover, false);
    dropbox.addEventListener("drop", droppics, false);

    //handles submission
    $("#inputdata").submit(function () {
        createLog();
        return false;
    });
    //Deletes rows when delete ID is clicked inside of table
    let imgDataTable = $("#imgDataTable");
    imgDataTable.on("click", "#delete", (event) => {
        $(event.currentTarget).closest('tr').remove();
        //reset row ids
        rowChange();
    });

    //Rotates image on click
    imgDataTable.on("click", "#imgRot", (event) => {
        let $this = $(event.currentTarget);
        let angle = parseInt($this.closest('tr').find('#imgAngle').text());
        angle += 90;
        if (angle >= 360) {
            angle = 0
        }
        // $(this).parent().css("webkitTransform", "rotate("+angle+"deg)");
        let img = $this.siblings('canvas')[0];
        rotateMe(img);
        $this.siblings('canvas').remove();
        $this.parent().prepend(rotateMe(img));
        $this.siblings('canvas').css({
            'max-height': '100%',
            'max-width': '100%',
            'position': 'absolute',
            'margin': 'auto',
            'top': 0,
            'left': 0,
            'right': 0,
            'bottom': 0
        });
        $this.closest('tr').find('#imgAngle').text(angle)
    });

    //makes table drag and drop sortable
    $("tbody").sortable({
        helper: fixHelper,
        update: rowChange
    }).disableSelection();

    // Save the log
    $("#saveLog").click(() => {
        let SN = $("#inputSiteName").val();
        if (SN.length === 0) {
            SN = "Unknown";
        }
        let PN = $("#inputProjectNumber").val();
        if (PN.length === 0) {
            PN = "Unknown";
        }
        exportTableToCSV($('#imgDataTable'), SN + "_" + PN + '_PhotoLog.csv');
    });

});

